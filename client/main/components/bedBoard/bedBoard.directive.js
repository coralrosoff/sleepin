(function () {
    angular
        .module('sleepIn.bedBoard')
        .directive('bedBoardDir', bedBoardDir);

    function bedBoardDir() {
        return {
            restrict: 'E',
            templateUrl: 'components/bedBoard/bedBoard.html',
            bindToController: true,
            controller: 'BedBoardCtrl',
            controllerAs: 'BedBoardCtrl'
        }
    }

})();