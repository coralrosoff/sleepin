(function () {
    angular
        .module('sleepIn.bedBoard')
        .controller('BedBoardCtrl', BedBoardCtrl);

    BedBoardCtrl.$inject = ['bedBoard', '$location', 'localStorageHandler', 'stringToDateParser'];
    
    function BedBoardCtrl(bedBoard, $location, localStorageHandler, stringToDateParser) {
        var vm = this;
        vm.GetAllBedsByGender;
        vm.userId = localStorageHandler.GetUserId();
        vm.userName = localStorageHandler.GetUserName();
        vm.BrowseToBedByOwnerId = BrowseToBedByOwnerId;
        vm.CheckUserId = CheckUserId;
        vm.DeleteOffer = DeleteOffer;
        vm.convertStringToDate = convertStringToDate;
        GetAllBeds(localStorageHandler.FindGender());

        function GetAllBeds(gender) {
            bedBoard.GetBedOffers(gender).then(function (AllBedsByGender) {
                vm.GetAllBedsByGender = AllBedsByGender;
            });
        }

        function convertStringToDate(string) {
            return stringToDateParser.ConvertStringToDate(string)
        }
        
        function BrowseToBedByOwnerId(owner_id, offer_id) {
            $location.path('/bedDetails').search({'owner_id': owner_id, 'offer_id': offer_id})
        }

        function CheckUserId(owner_id) {
            if (vm.userName == owner_id){
                return true;
            } else {
                
                return false
            }
        }
        
        function DeleteOffer(offer_id) {
           return bedBoard.CancelOffer(offer_id).then(function (status) {
               console.log(status);
               GetAllBeds()
           })
        }
        
        
    }
    
})();