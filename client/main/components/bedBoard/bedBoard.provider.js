(function () {
    angular
        .module('sleepIn.bedBoard')
        .service('bedBoard', bedBoard);

    bedBoard.$inject = ['$http'];
    
    function bedBoard($http) {
        return{
            GetBedOffers : GetBedOffers,
            CancelOffer : CancelOffer
        };
        
        function GetBedOffers(gender) {
            var request = {
                method: 'GET',
                url: '/api/offers/GetBedOffers?gender='+gender
            };
            return $http(request).then(function (response) {
                return response.data;
            }, function (err) {
                return err;
            });
        }

        function CancelOffer(offer_id) {
            var request = {
                method: 'POST',
                url: '/api/offers/CancelOffer',
                data: {offer_id:offer_id}
            };
            return $http(request).then(function (response) {
                return response.data;
            }, function (err) {
                return err;
            });
        }
    }
})();