(function () {
    angular
        .module('sleepIn.myRequests')
        .service('myRequests', myRequests);

    myRequests.$inject = ['$http'];

    function myRequests($http){
        return {
            GerPersonRequests : GerPersonRequests,
            CancelRequest : CancelRequest
        };
        
        function GerPersonRequests(owner_id) {
            var request = {
                method: 'GET',
                url: '/api/person/GetPersonRequests?user_id='+owner_id
            };
            return $http(request).then(function (response) {
                return response.data;
            },function (err) {
                return err
            });
        }

        function CancelRequest(requestId) {
            var request = {
                method: 'POST',
                url: '/api/requests/CancelRequest',
                data: {requestId: requestId}
            };
            return $http(request).then(function (response) {
                return response.data;
            }, function (err) {
                return err;
            });
        }
        
    }
})();