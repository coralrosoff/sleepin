(function () {
    angular
        .module('sleepIn.myRequests')
        .controller('MyRequestsCtrl', MyRequestsCtrl);

    MyRequestsCtrl.$inject= ['myRequests', 'localStorageHandler', 'stringToDateParser'];

    function MyRequestsCtrl(myRequests, localStorageHandler, stringToDateParser) {
        var vm = this;
        vm.MyRequests;
        vm.userId = localStorageHandler.GetUserId();
        vm.SendCancelRequest = SendCancelRequest;
        vm.convertStringToDate= convertStringToDate;
        GetMyRequests();

        function GetMyRequests() {
            return myRequests.GerPersonRequests(vm.userId).then(function (myRequests) {
                console.log(myRequests);
                vm.MyRequests = myRequests
            });
        }

        function SendCancelRequest(request_id) {
            return myRequests.CancelRequest(request_id).then(function (respond) {
                console.log(respond.status);
                GetMyRequests()
            });
        }

        function convertStringToDate(string) {
            return stringToDateParser.ConvertStringToDate(string)
        }
    }
})();
