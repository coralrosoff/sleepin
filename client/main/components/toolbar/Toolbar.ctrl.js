(function () {
    angular
        .module('sleepIn.toolbar')
        .controller('ToolbarCtrl', ToolbarCtrl);

    ToolbarCtrl.$inject = ['localStorageHandler', 'userInfoPage', 'sessionManager'];

    function ToolbarCtrl(localStorageHandler, userInfoPage, sessionManager) {
        var vm = this;
        vm.bedId = missingBed();

        vm.hasBed = sessionManager.hasBed;

        function missingBed() {
            var userId = localStorageHandler.GetUserId();
            userInfoPage.GetMyDetails(userId).then(function (details) {
                vm.bedId = details['bed_id'];
                if (!vm.bedId){
                    vm.bedId = true;
                } else {
                    vm.bedId = false
                }
            });
        }
    }
})();
