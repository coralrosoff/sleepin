( function() {
    angular
        .module('sleepIn.toolbar')
        .directive('toolbarDir', toolbarDir);

    function toolbarDir() {
        return {
            restrict: 'E',
            templateUrl: 'components/toolbar/toolbar.html',
            bindToController: true,
            controller: 'ToolbarCtrl',
            controllerAs: 'ToolbarCtrl'
        }
    }
})();