(function() {
    angular
        .module('sleepIn.login')
        .service('authentication', authentication);

    authentication.$inject = ['$http'];

    function authentication($http) {
        return {
            authenticateUser: authenticateUser
        };

        function authenticateUser(username, password, gender) {
            var request = {
                method: 'POST',
                url: '/api/login/AuthenticateUser',
                data: {username: username, password: password, gender: gender}
            };
            return $http(request).then(function (response) {
                return response.data;
            }, function(err) {
                return err;
            });
        }
    }
})();