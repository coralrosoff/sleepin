( function() {
    angular
        .module('sleepIn.login')
        .directive('loginDir', loginDir);

    function loginDir() {
        return {
            restrict: 'E',
            templateUrl: 'components/login/login.html',
            bindToController: true,
            controller: 'LoginCtrl',
            controllerAs: 'LoginCtrl'
        }
    }

})();