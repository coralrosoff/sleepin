(function () {
    angular
        .module('sleepIn.login')
        .controller('LoginCtrl', LoginCtrl);

    LoginCtrl.$inject = ['$location', 'authentication', '$scope', '$mdDialog', 'localStorageHandler', 'sessionManager', 'userInfoPage'];

    function LoginCtrl($location, authentication, $scope, $mdDialog, localStorageHandler, sessionManager, userInfoPage) {
        $scope.status = '';
        $scope.customFullscreen = false;
        $scope.isLoggedIn = isLoggedIn;
        $scope.username = username;
        $scope.showDialog = showDialog;
        $scope.submit = submit;
        $scope.logout = logout;

        function isLoggedIn() {
            return localStorageHandler.getItem('loggedUser') != undefined;
        }

        function username() {
            return localStorageHandler.getItem('loggedUser');
        }

        function showDialog(event) {
            $mdDialog.show({
                controller: LoginCtrl,
                templateUrl: 'components/login/loginDialog.html',
                parent: angular.element(document.body),
                targetEvent: event,
                clickOutsideToClose: false,
                escapeToClose: false,
                fullscreen: $scope.customFullscreen
            })
                .then(function () {
                    $scope.status = 'You cancelled the dialog.';
                });
        };

        function submit(username, password, gender) {
            if (username && password && gender) {
                authentication.authenticateUser(username, password, gender).then(function (response) {
                    if (response.status == "success") {
                        userInfoPage.GetMyDetails(response.soldier_id).then(function(details) {
                            if (details.bed) {
                                sessionManager.setBed(true);
                            } else {
                                sessionManager.setBed(false);
                            }
                        });
                        localStorageHandler.setItem('loggedUser', username);
                        localStorageHandler.setItem('gender', gender);
                        localStorageHandler.setItem('userId', response.soldier_id);
                        $mdDialog.hide();
                    }
                    else {
                        var dialogTitle = document.getElementById('dialog-title');
                        dialogTitle.innerText = "אחד הנתונים שהזנת שגוי";
                        dialogTitle.style.color = 'red';
                    }
                })
            }
        }

        function logout() {
            $location.path('/');
            localStorageHandler.removeItem('loggedUser');
            localStorageHandler.removeItem('gender');
            localStorageHandler.removeItem('userId');
        }
    }
})();