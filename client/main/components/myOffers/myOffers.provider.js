(function () {
    angular
        .module('sleepIn.myOffers')
        .service('myOffers', myOffers);

    myOffers.$inject = ['$http'];

    function myOffers($http) {
        return{
            GetPersonOffers : GetPersonOffers,
            CancelOffer:CancelOffer,
            RespondToRequest:RespondToRequest
        };

        function GetPersonOffers(user_id) {
            var request = {
                method: 'GET',
                url: '/api/offers/GetUserOffers?user_id='+user_id
            };
            return $http(request).then(function (response) {
                return response.data;
            }, function (err) {
                return err;
            });
        }
        function CancelOffer(offer_id) {
            var request = {
                method: 'POST',
                url: '/api/offers/CancelOffer',
                data: {offer_id: offer_id}
            };
            return $http(request).then(function (response) {
                return response.data;
            }, function (err) {
                return err;
            });
        }

        function RespondToRequest(request_id, response, offer_id) {
            console.log("the provider response is:",response);
            var request = {
                method: 'POST',
                url: '/api/requests/RespondToRequest',
                data: {requestId: request_id, response: response, offer_id: offer_id}
            };
            return $http(request).then(function (response) {
                return response.data;
            }, function (err) {
                return err;
            });
        }
    }
})();