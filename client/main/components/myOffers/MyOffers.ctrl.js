(function () {
    angular
        .module('sleepIn.myOffers')
        .controller('MyOffersCtrl', MyOffersCtrl);

    MyOffersCtrl.$inject = ['myOffers', 'localStorageHandler', 'stringToDateParser'];

    function MyOffersCtrl(myOffers, localStorageHandler, stringToDateParser) {
        var vm = this;
        vm.MyOffers;
        vm.userId = localStorageHandler.GetUserId();
        vm.CancelingOffer=CancelingOffer;
        vm.ChangeStringToDate = ChangeStringToDate;
        vm.ReplyToRequest = ReplyToRequest;
        GetMyOffers();
        
        function GetMyOffers() {
            return myOffers.GetPersonOffers(vm.userId).then(function (myOffers) {
                console.log(myOffers);
                vm.MyOffers = myOffers;
            })
        }
        function CancelingOffer(offer_id) {
            return myOffers.CancelOffer(offer_id).then(function (respond) {
                console.log(respond.status);
                GetMyOffers();
            })
        }
        
        function ChangeStringToDate(string) {
            return stringToDateParser.ConvertStringToDate(string)
        }
        
        function ReplyToRequest(request_id, response, offer_id) {
            return myOffers.RespondToRequest(request_id, response, offer_id).then(function (respond) {
                console.log(respond.status);
                GetMyOffers()
            })
        }
    }
})();