(function () {
    angular
        .module('sleepIn.bedOffer')
        .controller('BedOfferCtrl', BedOfferCtrl);

    BedOfferCtrl.$inject = ['bedOffer', 'userInfoPage', 'localStorageHandler', 'toastr'];

    function BedOfferCtrl(bedOffer, userInfoPage, localStorageHandler, toastr) {
        var vm = this;
        vm.startDate;
        vm.endDate;
        vm.bedDescription;
        vm.userId= localStorageHandler.GetUserId();
        vm.PersonDetails;
        vm.CreateOffer = CreateOffer;
        vm.MinimumDate = MinDate();
        vm.maximumDate= MaxDate();
        vm.FindBedInfo = FindBedInfo;
        vm.FindBedInfo(vm.userId);

        function CreateOffer() {
            if (vm.startDate && vm.endDate){
                return bedOffer.CreateOffer(vm.userId, vm.startDate.getTime(),vm.endDate.getTime()).then(function (status) {
                    console.log(status);
                    vm.startDate = undefined;
                    vm.endDate = undefined;
                    vm.bedDescription = undefined;
                    toastr.success('ההצעה התקבלה, תודה!');
                })
            }else {
                toastr.error('שכחת למלא את אחד הפרטים');
            }
        }
        
        function MinDate() {
            return new Date()
        }

        function MaxDate() {
            if (vm.startDate)
            {
                startDate = vm.startDate;
                startDate.setDate(startDate.getDate()+ 14);
                console.log(startDate);
                return startDate
            }
            else { return vm.startDate}
        }

        function FindBedInfo(domain_user) {
            return userInfoPage.GetMyDetails(domain_user).then(function (PersonDetails) {
                console.log(PersonDetails);
                vm.PersonDetails = PersonDetails;
                console.log(vm.PersonDetails)
            })
        }
        
    }
})();