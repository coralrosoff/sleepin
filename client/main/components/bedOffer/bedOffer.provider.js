(function () {
    angular
        .module('sleepIn.bedOffer')
        .service('bedOffer', bedOffer);

    bedOffer.$inject = ['$http'];

    function bedOffer($http) {
        return{
            CreateOffer : CreateOffer
        };

        function CreateOffer(owner_id, start_date, end_date) {
            var request = {
                method: 'POST',
                url: '/api/offers/CreateOffer',
                data: {owner_id: owner_id, begin_date: start_date, end_date: end_date}
            };
            return $http(request).then(function (response) {
                return response.data;
            }, function (err) {
                return err;
            });
        }
    }
})();