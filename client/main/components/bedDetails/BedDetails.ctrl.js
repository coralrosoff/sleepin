(function () {
    angular
        .module('sleepIn.bedDetails')
        .controller('BedDetailsCtrl', BedDetailsCtrl);

    BedDetailsCtrl.$inject = ['bedDetails','$location','localStorageHandler', 'toastr'];

    function BedDetailsCtrl(bedDetails,$location,localStorageHandler, toastr) {
        var vm = this;
        vm.startDate;
        vm.endDate;
        vm.AcceptBed = AcceptBed;
        gettingBedDetail();
        gettingOfferDetail();
        vm.GetOwnerByUrl=GetOwnerByUrl;
        vm.RequestingBed=RequestingBed;
        vm.details = "";
        vm.userId = localStorageHandler.GetUserId()


        function gettingBedDetail() {
            return bedDetails.GetBedDetails(GetOwnerByUrl()).then(function (bedDetails) {
                var details = bedDetails;
                vm.details=details;
            });
        }

        function gettingOfferDetail() {
            return bedDetails.GetOfferRequests(GetOfferByUrl()).then(function (offerDetails) {
                var offerdetails = offerDetails;
                vm.startoffer = new Date(offerdetails['begin_date']);
                vm.endoffer = new Date(offerdetails['end_date']);
                vm.offerdetails = offerdetails;

            });
        }
        function GetOwnerByUrl() {
            return $location.search()['owner_id']

        }
        function GetOfferByUrl() {
            return $location.search()['offer_id']

        }

        function AcceptBed() {
            console.log("");
        }

        function RequestingBed() {
            return bedDetails.RequestBed(GetOfferByUrl(),vm.userId, vm.startDate, vm.endDate)
                .then(function (response) {
                    vm.startDate = undefined;
                    vm.endDate = undefined;
                    toastr.success('בקשתך התקבלה');
                })
        }

    }
})();

