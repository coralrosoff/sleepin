(function () {
    angular
        .module('sleepIn.bedDetails')
        .service('bedDetails', bedDetails);

    bedDetails.$inject = ['$http'];

    function bedDetails($http) {
        return{
            GetBedDetails : GetBedDetails,
            RequestBed : RequestBed,
            GetOfferRequests:GetOfferRequests
        };

        function GetBedDetails(owner_id) {
            var request = {
                method: 'GET',
                url: '/api/beds/GetLimitedBedDetails?owner_id='+owner_id
            };
            return $http(request).then(function (response) {
                return response.data;
            }, function (err) {
                return err;
            });
        }
        function GetOfferRequests(offer_id) {
            var request = {
                method: 'GET',
                url: '/api/offers/GetOfferRequests?offer_id='+offer_id
            };
            return $http(request).then(function (response) {
                return response.data;
            }, function (err) {
                return err;
            });

        }

        function RequestBed(offerid,userid,begindate,enddate) {
            console.log(userid);
            var request = {
                method: 'POST',
                url: '/api/requests/RequestBed',
                data: {offerId:offerid, userId:userid , beginDate :(begindate.getTime()), endDate:(enddate.getTime())}
            };
            return $http(request).then(function (response) {
                return response.data;
            }, function (err) {
                return err;
            });
        }

    }
})();
