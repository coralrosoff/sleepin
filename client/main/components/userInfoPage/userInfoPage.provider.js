(function () {
    angular
        .module('sleepIn.userInfoPage')
        .service('userInfoPage', userInfoPage);

    userInfoPage.$inject = ['$http'];
    
    function userInfoPage($http) {
        return{
            GetMyDetails: GetMyDetails,
            BedManager: BedManager
        };
        
        function GetMyDetails(user_id) {
            var request = {
                method: 'GET',
                url: '/api/person/GetMyDetails?user_id='+user_id
            };
            return $http(request).then(function (response) {
                return response.data;
            },function (err) {
                return err
            });
        }

        function BedManager(bed) {
            var request = {
                method: 'POST',
                url: '/api/beds/BedManager',
                data: {bedObject: bed}
            };
            return $http(request).then(function (response) {
                return response.data;
            },function (err) {
                return err
            });
        }


    }
    
})();