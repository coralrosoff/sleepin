(function () {
    angular
        .module('sleepIn.userInfoPage')
        .controller('UserInfoPageCtrl', UserInfoPageCtrl);

    UserInfoPageCtrl.$inject = ['userInfoPage', 'localStorageHandler', 'toastr', 'sessionManager'];

    function UserInfoPageCtrl(userInfoPage, localStorageHandler, toastr, sessionManager) {
        var vm = this;
        vm.ownerId = localStorageHandler.GetUserId();
        vm.PersonDetails;
        vm.WantToChange;
        vm.newRoom;
        vm.newBed;
        vm.newBuilding;
        vm.newDescription;
        vm.GetDetails = GetDetails;
        vm.EditBed = EditBed;
        vm.WantToChangeDetails = WantToChangeDetails;
        GetDetails(vm.ownerId);
        vm.missingBed = missingBed;
        vm.hasBed = sessionManager.hasBed;

        function missingBed() {
            var userId = localStorageHandler.GetUserId();
            userInfoPage.GetMyDetails(userId).then(function (details) {
                vm.bedId = details['bed_id'];
                if (vm.bedId) {
                    return false;
                } else {
                    return true;
                }
            });
        }

        function GetDetails(user_id) {
            return userInfoPage.GetMyDetails(user_id).then(function (PersonDetails) {
                console.log(PersonDetails);
                vm.PersonDetails = PersonDetails
            })
        }
        
        function WantToChangeDetails() {
            vm.WantToChange = true
        }

        function EditBed(bed_id) {
            var NewBed = {owner_id: vm.ownerId,bed_id: bed_id, room_number: vm.newRoom, bed_number: vm.newBed, building_id: vm.newBuilding, description: vm.newDescription};
            console.log(NewBed);
            if (vm.ownerId && vm.newDescription && vm.newRoom && vm.newBed && vm.newBuilding){
                return userInfoPage.BedManager(NewBed).then(function (respond) {
                    console.log(respond.status);
                    vm.newRoom = undefined;
                    vm.newBed = undefined;
                    vm.newBuilding = undefined;
                    vm.newDescription = undefined;
                    vm.WantToChange = false;
                    GetDetails(vm.ownerId);
                    vm.hasBed = true;
                    sessionManager.setBed(true);
                })

            } else {
                toastr.error('לא מילאת את הפרטים כראוי!');
            }
        }
        
    }
})();