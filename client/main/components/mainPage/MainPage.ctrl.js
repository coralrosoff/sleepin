(function() {
    angular
        .module('sleepIn.mainPage', ['ngMaterial'])
        .controller('MainPageCtrl', MainPageCtrl);

    MainPageCtrl.$inject = ['userInfoPage', 'localStorageHandler', '$location', '$anchorScroll', '$interval', 'sessionManager'];
    function MainPageCtrl(userInfoPage, localStorageHandler, $location, $anchorScroll, $interval, sessionManager) {
        var vm = this;
        vm.bedId = missingBed();
        vm.scrollTo = scrollTo;
        vm.toCircle=true;
        vm.determinateValue=0;
        vm.hasBed = sessionManager.hasBed;

        function missingBed() {
            var userId = localStorageHandler.GetUserId();
            userInfoPage.GetMyDetails(userId).then(function (details) {
                vm.bedId = details['bed_id'];
                if (!vm.bedId){
                    vm.bedId = true;
                } else {
                    vm.bedId = false
                }
            });
        }

        function scrollTo(id) {
            $location.hash(id);
            $anchorScroll();
        }

        $interval(function() {
            vm.determinateValue += 1;
            if (vm.determinateValue > 12) {
                vm.toCircle=false;
            }
        }, 100);
    }
})();