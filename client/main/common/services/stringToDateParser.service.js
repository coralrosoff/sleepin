(function () {
    angular
        .module('sleepIn.commonServices')
        .service('stringToDateParser',stringToDateParser);
    
    function stringToDateParser() {
        return{
            ConvertStringToDate: ConvertStringToDate
        };

        function ConvertStringToDate(string) {
            var date = new Date(string);
            return date.toDateString()
        }
        
    }
    
})();