(function() {
    angular
        .module('sleepIn.commonServices')
        .service('sessionManager', sessionManager);

    function sessionManager() {

        var _hasBed;

        return {
            hasBed: hasBed,
            setBed: setBed
        };

        function hasBed() {
            return _hasBed;
        }

        function setBed(hasBed) {
            _hasBed = hasBed;
        }
    }
})();