
(function () {
    angular
        .module('sleepIn.commonServices')
        .service('localStorageHandler',localStorageHandler);

    function localStorageHandler() {
        return {
            setItem: setItem,
            FindGender: FindGender,
            GetUserId: GetUserId,
            getItem: getItem,
            GetUserName:GetUserName,
            removeItem:removeItem
        };

        function setItem(key, value) {
            var toSaveInLocalStorage = JSON.stringify(value);
            localStorage.setItem(key, toSaveInLocalStorage);
        }

        function getItem(key) {
            var fromLocalStorage = localStorage.getItem(key);
            if (!fromLocalStorage) {
                return undefined;
            }
            return JSON.parse(fromLocalStorage);
        }
        
        function removeItem(key) {
            localStorage.removeItem(key)
        }
        
        function FindGender() {
            return getItem('gender');
        }

        function GetUserId() {
            return parseInt(getItem('userId'))
        }

        function GetUserName() {
            return getItem('loggedUser')
        }
    }
})();