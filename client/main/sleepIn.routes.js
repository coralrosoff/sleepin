(function () {
    angular
        .module('sleepIn')
        .config(routeConfig);

    routeConfig.$inject = ['$routeProvider'];

    function routeConfig($routeProvider) {
        $routeProvider
            .when('/', {
                controller: 'MainPageCtrl',
                controllerAs: 'MainPageCtrl',
                templateUrl: 'components/mainPage/mainPage.html'
            })
            
            .when('/myOffers', {
                controller: 'MyOffersCtrl',
                controllerAs: 'MyOffersCtrl',
                templateUrl: 'components/myOffers/myOffers.html'
            })
            
            .when('/myRequests', {
                controller: 'MyRequestsCtrl',
                controllerAs: 'MyRequestsCtrl',
                templateUrl: 'components/myRequests/myRequests.html'
            })
        
            .when('/userInfoPage', {
                controller: 'UserInfoPageCtrl',
                controllerAs: 'UserInfoPageCtrl',
                templateUrl: 'components/userInfoPage/userInfoPage.html'
            })
        
            .when('/bedDetails', {
                controller: 'BedDetailsCtrl',
                controllerAs: 'BedDetailsCtrl',
                templateUrl: 'components/bedDetails/bedDetails.html'
            })
        
            .when('/bedOffer',{
                controller: 'BedOfferCtrl',
                controllerAs: 'BedOfferCtrl',
                templateUrl: 'components/bedOffer/bedOffer.html'
            })

    }
})();