/**
 * Created by user on 21/09/2016.
 */
module.exports = resolveAndResponseMiddleware;

function resolveAndResponseMiddleware(req, res, next) {
    res.resolveAndResponse = function (promise) {
        console.log("the promise is",promise);
        promise
            .then(function (result) {
                res.json(result);
            }, function (err) {
                next(err);
            });
    };
    next();
}