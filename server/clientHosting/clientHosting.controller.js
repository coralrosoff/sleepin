module.exports = {
    index: mainPage
};

var fs = require('fs');
var path = require('path');

function mainPage(req, res, next) {
    var headers = {
        'Content-Type': 'text/html'
    };

    var file = fs.readFileSync(path.join(__dirname, '../../client/index.html'));

    return {
        status: 200,
        headers: headers,
        index: file
    };
}