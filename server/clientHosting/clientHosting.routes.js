var express = require('express');
var controller = require('./clientHosting.controller.js');

var router = express.Router();

router.get('/', index);

function index(req, res, next) {
    var data = controller.index();
    res.writeHead(data.status, data.headers);
    res.end(data.index);
}

module.exports = router;
