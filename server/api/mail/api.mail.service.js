/**
 * Created by user on 21/09/2016.
 */
module.exports = MailService;
var Q = require('q');
var nodemailer = require('nodemailer');
var config = require('../../config');

function MailService() {
    this.SendNewRequest = SendNewRequest;
    this.SendRequestAccepted = SendRequestAccepted;
    this.SendRequestDeclined = SendRequestDeclined;
    this.SendRequestCanceled = SendRequestCanceled;

    var transporter = nodemailer.createTransport(config.smtpConfig);

    function SendNewRequest(ownerUser, requesterUser, beginDate, endDate, subject, text){
        var mailOptions = {
            from: config.smtpConfig.auth.user,
            to: ownerUser,
            subject: subject,
            text: text
        };
        return transporter.sendMail(mailOptions, function(err, info){
            return info.response
        });
    }

    function SendRequestAccepted(requesterUser, beginDate, endDate, subject, text){
        var mailOptions = {
            from: config.smtpConfig.auth.user,
            to: requesterUser,
            subject: subject,
            text: text
        };
        return transporter.sendMail(mailOptions, function(err, info){
            return info.response
        });
    }

    function SendRequestDeclined(requesterUser, beginDate, endDate, subject, text){
        var mailOptions = {
            from: config.smtpConfig.auth.user,
            to: requesterUser,
            subject: subject,
            text: text
        };
        return transporter.sendMail(mailOptions, function(err, info){
            return info.response
        });
    }

    function SendRequestCanceled(ownerUser, requesterUser, beginDate, endDate, subject, text){
        var mailOptions = {
            from: config.smtpConfig.auth.user,
            to: ownerUser,
            subject: subject,
            text: text
        };
        return transporter.sendMail(mailOptions, function(err, info){
            return info.response
        });
    }
}



var transporter = nodemailer.createTransport(config.smtpConfig);

function testMail() {
    var mailOptions = {
        from: 'rone@domaindom',
        to: '117alony@domaindom',
        subject: 'test',
        text: 'hello world'
    };
    return transporter.sendMail(mailOptions, function (err, info) {
        if (err) {
            console.log(err);
            return err;
        }
        else {
            return info.response;
        }
    });
}

testMail();