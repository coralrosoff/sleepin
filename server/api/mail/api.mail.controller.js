/**
 * Created by user on 21/09/2016.
 */

module.exports = MailController;
var Q = require('q');
var MailService = require('./api.mail.service.js');

function MailController() {

    var service = new MailService();

    this.SendNewRequest = SendNewRequest;
    this.SendRequestAccepted = SendRequestAccepted;
    this.SendRequestDeclined = SendRequestDeclined;
    this.SendRequestCanceled = SendRequestCanceled;


    function SendNewRequest(ownerUser, requesterUser, beginDate, endDate) {
        var deferred = Q.defer();
        var subject = 'קיבלת בקשה ב-SleepIn!';
        var text = 'קיבלת בקשה מ' + requesterUser + ' על הצעת המיטה שלך בין התאריכים ' + beginDate + '-' + endDate + '.' + '<br>' + 'היכנס/י לאתר על מנת לצפות בפרטי הבקשה המלאים ולהגיב.';
        var isSuccessful = service.SendNewRequest(ownerUser, requesterUser, beginDate, endDate, subject, text);
        deferred.resolve(isSuccessful);
        return deferred.promise;
    }

    function SendRequestAccepted(requesterUser, beginDate, endDate) {
        var deferred = Q.defer();
        var subject = 'הבקשה שלך ב-SleepIn אושרה!';
        var text = 'הבקשה ששלחת עבור המיטה של ' + ownerUser + ' בין התאריכים ' + beginDate + '-' + endDate + ' אושרה!' + '<br>' + 'באפשרותך ליצור עימו/עימה קשר באמצעות ה-voip: ' + ownerVoip + ' או הפלאפון: ' + ownerPhone + '.';
        var isSuccessful = service.SendRequestAccepted(requesterUser, beginDate, endDate, subject, text);
        deferred.resolve(isSuccessful);
        return deferred.promise;
    }

    function SendRequestDeclined(requesterUser, beginDate, endDate) {
        var deferred = Q.defer();
        var subject = 'הבקשה שלך ב-SleepIn נדחתה';
        var text = 'הבקשה ששלחת עבור המיטה של ' + ownerUser + ' בין התאריכים ' + beginDate + '-' + endDate +' נדחתה.' + '<br>' + 'נסה/י למצוא באתר הצעות נוספות שיתאימו לך!';
        var isSuccessful = service.SendRequestDeclined(requesterUser, beginDate, endDate, subject, text);
        deferred.resolve(isSuccessful);
        return deferred.promise;
    }

    function SendRequestCanceled(ownerUser, requesterUser, beginDate, endDate) {
        var deferred = Q.defer();
        var subject = 'אחת הבקשות שנשלחו אליך ב-SleepIn בוטלה';
        var text = 'בקשת המיטה שנשלחה אליך ע"י ' + requesterUser + ' בין התאריכים ' + beginDate + '-' + endDate + ' בוטלה.';
        var isSuccessful = service.SendRequestCanceled(ownerUser, requesterUser, beginDate, endDate, subject, text);
        deferred.resolve(isSuccessful);
        return deferred.promise;
    }
}
