/**
 * Created by user on 22/09/2016.
 */
module.exports={GetMyDetails: "SELECT * FROM sleepin.dbo.soldiers WHERE soldier_id=@soldierId",
    GetPersonBed: "SELECT beds.bed_id, beds.bed_description, buildings.name building_name, beds.room_number,\
                          beds.bed_number, beds.last_modify_date\
                   FROM sleepin.dbo.beds beds, sleepin.dbo.buildings buildings\
                   WHERE beds.building_id = buildings.building_id\
                   AND beds.bed_id IN (SELECT bed_id from soldiers where soldier_id = @soldierId)",
    GetPersonDetails: "SELECT soldier_id, first_name, last_name, display_name, email, phone, voip, gender,\
    creation_date, last_modify_date FROM sleepin.dbo.soldiers WHERE soldier_id = @soldierId",
    GetPersonRequests: "SELECT * FROM sleepin.dbo.requests\
                        WHERE requester_id = @soldierId AND request_status != 'CANCELED'"
};
