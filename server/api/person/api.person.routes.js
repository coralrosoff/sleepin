/**
 * Created by user on 21/09/2016.
 */

module.exports = PersonRouter;
var PersonController = require('./api.person.controller.js');

function PersonRouter(){

    var controller = new PersonController();
    var express = require('express');
    var router = express.Router();

    //POST

    //GET
    router.get('/GetPersonDetails',GetPersonDetails);
    router.get('/GetPersonRequests',GetPersonRequests);
    router.get('/GetMyDetails',GetMyDetails);


    function GetMyDetails(req,res){
        var userId = req.query.user_id;
        res.resolveAndResponse(controller.GetMyDetails(userId));
    }

    function GetPersonRequests(req,res){
        var userId = req.query.user_id;
        res.resolveAndResponse(controller.GetPersonRequests(userId));
    }


    function GetPersonDetails(req,res){
        var userId = req.query.user_id;
        res.resolveAndResponse(controller.GetPersonDetails(userId));
    }

    this.router = router;
}

