/**
 * Created by user on 21/09/2016.
 */
module.exports = PersonService;
var Q = require('q');
var queries = require('./api.person.queries');
var config = require('../../config');
var sql = require('mssql');

function PersonService() {
    this.GetMyDetails = GetMyDetails;
    this.GetPersonDetails = GetPersonDetails;
    this.GetPersonRequests = GetPersonRequests;
    this.GetPersonBed = GetPersonBed;

    var mssqlConnectionConfig = config.mssqlConnectionDetails;
    var connection = new sql.Connection(mssqlConnectionConfig);
    connection.connect();

    function GetMyDetails(userId){
        var myDetails = new sql
            .Request(connection)
            .input('soldierId',sql.Int(),userId)
            .query(queries.GetMyDetails);
        return myDetails.then(function(result){
            return result[0];
        });
    }

    function GetPersonDetails(userId){
        var personDetails = new sql
            .Request(connection)
            .input('soldierId',sql.Int(),userId)
            .query(queries.GetPersonDetails);
        return personDetails.then(function(result){
            return result[0];
        });
    }

    function GetPersonBed(userId){
        var personDetails = new sql
            .Request(connection)
            .input('soldierId',sql.Int(),userId)
            .query(queries.GetPersonBed);
        return personDetails.then(function(result){
            return result[0];
        });
    }

    function GetPersonRequests(userId){
        var personRequests = new sql
            .Request(connection)
            .input('soldierId',sql.Int(),userId)
            .query(queries.GetPersonRequests);
        return personRequests.then(function(result){
            return result;
        });
    }
}