/**
 * Created by user on 21/09/2016.
 */

module.exports = PersonController;
var Q = require('q');
var PersonService=require('./api.person.service.js');

function PersonController(){
  
    var service = new PersonService();
    this.GetPersonDetails= GetPersonDetails;
    this.GetMyDetails=GetMyDetails;
    this.GetPersonRequests=GetPersonRequests;


    function GetMyDetails(userId){
        var deferred = Q.defer();
        var myDetails=service.GetMyDetails(userId);
        var myBed=service.GetPersonBed(userId);
        Q.all([myDetails,myBed]).spread(function(details,bed){
            console.log(details);
            details.bed=bed;
            deferred.resolve(details);
        });
        return deferred.promise;
    }

    function GetPersonRequests(userId){
        var deferred = Q.defer();
        var personRequests=service.GetPersonRequests(userId);
        deferred.resolve(personRequests);
        return deferred.promise;
    }

    //get another person details - td:  get only the details i can show!!!
    function GetPersonDetails(userId){
        var deferred = Q.defer();
        var personDetails=service.GetPersonDetails(userId);
        deferred.resolve(personDetails);
        return deferred.promise;
    }
    
    
}