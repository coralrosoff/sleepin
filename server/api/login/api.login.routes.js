/*
 * Created by user on 21/09/2016.
*/

module.exports = LoginRouter;
var LoginController = require('./api.login.controller.js');

function LoginRouter(){
    
    var controller = new LoginController();
    var express = require('express');
    var router = express.Router();
    
    
    //POST
    router.post('/AuthenticateUser',AuthenticateUser);

    //GET
    router.get('/GetUsersByPrefix',GetUsersByPrefix);
    router.get('/GetUserByUserName',GetUserByUserName);
    router.get('/AuthenticateUser',AuthenticateUser);

    

    function GetUsersByPrefix(req,res,next){
        var displayName = req.query.displayName;
        controller.GetUsersByPrefix(displayName,function(error,data){
            if(error) {
                console.log(error);
            }else{
                res.json(data);
            }
        });
    }

    function GetUserByUserName(req,res,next){
        var userName = req.query.userName;
        console.log(userName);
        controller.GetUserByUserName(userName,function(error,data){
            if(error) {
                console.log(error);
            }else{
                res.json(data);
            }
        });
    }

    function AuthenticateUser(req,res,next){
        var username=req.body.username;
        var password=req.body.password;
        var gender=req.body.gender;
        res.resolveAndResponse(controller.AuthenticateUser(res,username,password,gender));
        /*
        if(password==='12345'){
            res.statusCode=200;
            res.send("YoPi Do");
            
        }
        else {
            res.statusCode=401;
            res.send("Incorrect Password Men/Women");
            //res.resolveAndResponse(controller.AuthenticateUser(username, password));
        }*/
    }

    this.router = router;
}

