/**
 * Created by user on 22/09/2016.
 */
module.exports={
    CheckIfUserExist:"\
    select * from soldiers where user_name=@userName",
    FirstLogin:
     "insert into sleepin.dbo.soldiers(first_name,last_name,email,display_name,user_name,gender,creation_date,last_modify_date)\
     values(@firstname,@lastname,@mail,@displayname,@username,@gender,GETDATE(),GETDATE())",
    CheckGender:"\
    select gender from soldiers where soldier_id=@ownerId",
    SetGender:"\
    update soldiers set gender=@gender where soldier_id=@ownerId",
    GetSoldierId:"\
    select soldier_id from soldiers where user_name=@username"


};