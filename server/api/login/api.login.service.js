/**
 * Created by user on 21/09/2016.
 */

module.exports = LoginService;
var Q = require('q');
var queries = require('./api.login.queries');
var passport=require('passport');
var config = require('../../app.config.js');
var windowsStrategy=require('passport-windowsauth');

var sql = require('mssql');

function LoginService() {

    var  mssqlConnectionConfig = config.mssqlConnectionDetails;
    var connection = new sql.Connection(mssqlConnectionConfig);
    connection.connect();

    this.CheckIfUserExist=CheckIfUserExist;
    this.login= login;
    this.FirstLogin=FirstLogin;
    this.CheckGender=CheckGender;
    this.SetGender=SetGender;
    this.GetSoldierId=GetSoldierId;
    this.OldUserConnection=OldUserConnection;

    function init(){
        passport.use(new windowsStrategy(config.authentication,
        function(profile,done){
            if(profile===null){
                done(undefined,profile);
            }
            else{
                done(undefined,profile);
            }
        }));
    }
    init();

    function login(userName,password){
        var deferred = Q.defer();
        var body = {
            body: {
                username:userName,
                password:password
            }
        };
        passport.authenticate('WindowsAuthentication',function(err,userData){
            if(err){
                return deferred.reject(err);
            }
            if(!userData){
                console.log("authenticate failed!");
                return deferred.resolve({
                   status:"failed"
                });
            }else{
                return deferred.resolve({
                    status:"success",
                    displayName:userData.displayName,
                    givenName:userData.name.givenName,
                    lastName: userData.name.familyName,
                    mail:userData.emails[0]
                });
            }
        })(body,function(body) {
            return deferred.reject(body);
        },function (body){
            return deferred.reject(body);
        });
        return deferred.promise;
    }

    function CheckIfUserExist(userName){
        return new sql
            .Request(connection)
            .input('userName',sql.NVarChar(50),userName)
            .query(queries.CheckIfUserExist);
    }

    function FirstLogin(firstname,lastname,mail,displayname,username,gender){
        var query= new sql
            .Request(connection)
            .input('firstname',sql.NVarChar(50),firstname)
            .input('lastname',sql.NVarChar(50),lastname)
            .input('mail',sql.NVarChar(50),mail)
            .input('displayname',sql.NVarChar(50),displayname)
            .input('username',sql.NVarChar(50),username)
            .input('gender',sql.NVarChar(50),gender)
            .query(queries.FirstLogin);
            return query.then(function(){
                 return GetSoldierId(username).then(function(result) {
                     return result;
                 });
        });
    }

    function OldUserConnection(){

    }

    function CheckGender(ownerId){
        var gender= new sql
            .Request(connection)
            .input('ownerId',sql.Int(),ownerId)
            .query(queries.CheckGender);
        return gender.then(function(result){
            return result[0].gender;
        });
    }
    function SetGender(gender,ownerId){
        return new sql
            .Request(connection)
            .input('gender',sql.NVarChar(20),gender)
            .input('ownerId',sql.Int(),ownerId)
            .query(queries.SetGender);
    }


    function GetSoldierId(username){
        return new sql
            .Request(connection)
            .input('username',sql.NVarChar(50),username)
            .query(queries.GetSoldierId);
        // soldier_id.then(function(value){
        //     console.log("this is the value",value);
        //    return value;
        // });
    }


    
}

