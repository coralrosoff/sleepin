/**
 * Created by user on 21/09/2016.
 */

module.exports = BedsController;
var Q = require('q');
var BedsService=require('./api.beds.service');

function BedsController(){

    var service = new BedsService();
    this.GetLimitedBedDetails=GetLimitedBedDetails;
    this.EditBed=EditBed;
    this.AddBed=AddBed;
    this.GetAllBuildings=GetAllBuildings;

    function GetLimitedBedDetails(ownerId){
        var deferred = Q.defer();
        var bedDetails=service.GetLimitedBedDetails(ownerId);
        deferred.resolve(bedDetails);
        return deferred.promise;
    }

    function GetAllBuildings(){
        var deferred = Q.defer();
        var buildings=service.GetAllBuildings();
        deferred.resolve(buildings);
        return deferred.promise;
    }


    function EditBed(bedObject){
        var deferred = Q.defer();
        var bedId=bedObject.bed_id;
        var description=bedObject.description;
        var buildingId=bedObject.building_id;
        var roomNumber=bedObject.room_number;
        var bed_number=bedObject.bed_number;
        var isSuccessful=service.EditBed(bedId,description,buildingId,roomNumber,bed_number);
        deferred.resolve(isSuccessful);
        return deferred.promise;
    }

    function AddBed(bedObject){
        var deferred = Q.defer();
        var ownerId=bedObject.owner_id;
        var description=bedObject.description;
        var buildingId=bedObject.building_id;
        var roomNumber=bedObject.room_number;
        var bed_number=bedObject.bed_number;
        var isAlreadyHasBed=service.CheckIfUserHaveBed(ownerId);
        return isAlreadyHasBed.then(function(value){
            console.log("the value is: ",value);
           if(value[0].bed_id===null){
                console.log("entered");
                var addBedPromise=service.AddBed(ownerId,description,buildingId,roomNumber,bed_number);
                return addBedPromise.then(function(bedId){
                        console.log("also entered");
                        var promise = service.UpdateSoldierBed(ownerId,bedId);


                        deferred.resolve(promise);
                        return deferred.promise;
                });
            }else{
                deferred.reject("Fatal Error , user already has bed");
                return deferred.promise;
            }
        });
    }
}

