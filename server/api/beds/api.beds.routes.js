/**
 * Created by user on 21/09/2016.
 */

module.exports = BedsRouter;
var BedsController = require('./api.beds.controller.js');

function BedsRouter(){

    var controller = new BedsController();
    var express = require('express');
    var router = express.Router();
    //POST
    router.post('/BedManager',BedManager);

    //GET
    router.get('/GetLimitedBedDetails',GetLimitedBedDetails);
    router.get('/GetAllBuildings',GetAllBuildings);


    function GetLimitedBedDetails(req,res,next){
        var ownerId = req.query.owner_id;
        res.resolveAndResponse(controller.GetLimitedBedDetails(ownerId));
    }




    function GetAllBuildings(req,res,next){
        res.resolveAndResponse(controller.GetAllBuildings());
    }



    function BedManager(req,res,next){
        var bedObject=req.body.bedObject;
        if(bedObject) {
            if(bedObject.bed_id){
                res.resolveAndResponse(controller.EditBed(bedObject));
            }else{
                res.resolveAndResponse(controller.AddBed(bedObject));
            }
        }else{
            console.log("No data given!!");
            res.sendStatus(404);
        }
    }

    this.router = router;
}
