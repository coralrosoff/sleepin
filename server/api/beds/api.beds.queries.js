/**
 * Created by user on 22/09/2016.
 */
module.exports={
    getBedDetails:"\
      select beds.bed_id,beds.bed_description from sleepin.dbo.beds beds,sleepin.dbo.soldiers soldiers\
       where soldiers.soldier_id=@ownerId \
    and beds.bed_id=soldiers.bed_id",
    updateBedDetails:"\
    update sleepin.dbo.beds set bed_description=@description,building_id=@buildingId,room_number=@roomNumber,bed_number=@bed_number,last_modify_date=@last_modify_date\
    where bed_id=@bedId",
    getOwnerDetails:"\
    select soldiers.soldier_id as user_id,soldiers.first_name,soldiers.last_name,soldiers.display_name,soldiers.email,\
    soldiers.phone,soldiers.gender,soldiers.voip\
    from sleepin.dbo.soldiers soldiers\
    where soldiers.soldier_id=@ownerId",
    InsertNewBed:"\
    insert into sleepin.dbo.beds (bed_description,building_id,room_number,bed_number,creation_date,last_modify_date,status,owner_id)values \
    (@description,@buildingId,@roomNumber,@bed_number,@create_date,@last_modify_date,'CURRENT',@ownerId)",
    GetBedIdByOwnerId:"\
    select bed_id from sleepin.dbo.beds where owner_id=@ownerId",
    UpdateSoldierBed:"\
    update sleepin.dbo.soldiers set bed_id=@bedId where soldier_id=@ownerId",
    CheckIfUserHaveBed:"\
    select bed_id from sleepin.dbo.soldiers where soldier_id=@ownerId",
    GetAllBuildings:"\
    select * from sleepin.dbo.buildings"
};