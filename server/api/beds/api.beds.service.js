/**
 * Created by user on 21/09/2016.
 */
module.exports = BedsService;
var Q = require('q');
var queries = require('./api.beds.queries');

var config = require('../../config');
var sql = require('mssql');
function BedsService() {

    this.GetLimitedBedDetails=GetLimitedBedDetails;
    this.EditBed=EditBed;
    this.AddBed=AddBed;
    this.UpdateSoldierBed=UpdateSoldierBed;
    this.CheckIfUserHaveBed=CheckIfUserHaveBed;
    this.GetAllBuildings=GetAllBuildings;


   var  mssqlConnectionConfig = config.mssqlConnectionDetails;
    var connection = new sql.Connection(mssqlConnectionConfig);
    connection.connect();

    function GetLimitedBedDetails(ownerId){
        var bedDetails = new sql
            .Request(connection)
            .input('ownerId',sql.Int(),ownerId)
            .query(queries.getBedDetails);
        var ownerDetails=new sql
            .Request(connection)
            .input('ownerId',sql.Int,ownerId)
            .query(queries.getOwnerDetails);
       return  ownerDetails.then(function(ownerdetails){
          return  bedDetails.then(function(bed){
              console.log(bed[0]);
              bed[0].owner=ownerdetails[0];
               return bed[0];
            });
        });
    }
    
    function EditBed(bedId,description,buildingId,roomNumber,bed_number){
        var now = new Date();
        var nowForamted= now.toLocaleDateString();
        return new sql
            .Request(connection)
            .input('bedId',sql.Int(),bedId)
            .input('description',sql.NVarChar(50),description)
            .input('buildingId',sql.Int(),buildingId)
            .input('roomNumber',sql.Int(),roomNumber)
            .input('bed_number',sql.Int(),bed_number)
            .input('last_modify_date',sql.Date,nowForamted)
            .query(queries.updateBedDetails);
    }

    function AddBed(ownerId,description,buildingId,roomNumber,bed_number){
        var now = new Date();
        var nowForamted= now.toLocaleDateString()
        var insertQuery = new sql
            .Request(connection)
            .input('ownerId',sql.Int(),ownerId)
            .input('description',sql.NVarChar(50),description)
            .input('buildingId',sql.Int(),buildingId)
            .input('roomNumber',sql.Int(),roomNumber)
            .input('bed_number',sql.Int(),bed_number)
            .input('create_date',sql.Date,nowForamted)
            .input('last_modify_date',sql.Date,nowForamted)
            .query(queries.InsertNewBed);
            return insertQuery.then(function(){
                console.log("owner id in Add bed is:",ownerId);
            return GetBedIdByOwnerId(ownerId)


        });
    }

    function GetBedIdByOwnerId(ownerId){
        console.log("the owner to search is: ",ownerId);
        var promise = new sql
            .Request(connection)
            .input('ownerId',sql.Int(),ownerId)
            .query(queries.GetBedIdByOwnerId);
        return promise.then(function(data){
            console.log("ths data is:",data);
            return data[0].bed_id;
        });
    }

    function UpdateSoldierBed(ownerId,BedId){
        console.log("going to update soldier,",ownerId,"bed id is:",BedId);
        return new sql
            .Request(connection)
            .input('ownerId',sql.Int,ownerId)
            .input('BedId',sql.Int,BedId)
            .query(queries.UpdateSoldierBed);
    }
    
    function CheckIfUserHaveBed(ownerId){
        return new sql
            .Request(connection)
            .input('ownerId',sql.Int,ownerId)
            .query(queries.CheckIfUserHaveBed);
    }

    function GetAllBuildings(){
        return new sql
            .Request(connection)
            .query(queries.GetAllBuildings);
    }

}





/*
connection.connect().then(function(){
    sql.query`select * from beds`.then(function(recordset) {
        console.log(recordset);
    }).catch(function(err){
        console.log(err);
    });
    });
*/




/*

function BedsService() {
    var sample_bed_1 = {
        bedId: 1,
        description: "the best bed in town",
        owner: {
            user_id: 1,
            first_name: "ben",
            last_name: "hagag",
            display_name: "ben hagever",
            email: "user.domaindom",
            phone: 0526222500, 
            voip: 7888,
            gender: "m"
        }
    }

    var sample_bed_2 = {
        bedId: 1,
        description: "the worst bed in town",
        owner: {
            user_id: 2,
            first_name: "shmolik",
            last_name: "pinhasov",
            display_name: "pinishmolik",
            email: "shmolik.domaindom",
            phone: 1234567,
            voip: 7190,
            gender: "m"
        }
    }


    this.getBedsDetails=getBedsDetails;

    function getBedsDetails(ownerId){
        return sample_bed_2;
    }


    var bedsoffers = [
        {
            offer_id: 1,
            owner_id: 1,
            start_date: Date(),
            end_date: Date(),
            status: "Approved",
            requests: [{request_id: 1, start_date: Date(), end_date: Date()}]
        },
        {
            offer_id: 2,
            owner_id: 2,
            start_date: Date(),
            end_date: Date(),
            status: "Canceled",
            requests: [{request_id: 2, start_date: Date(), end_date: Date()}]
        },
        {
            offer_id: 3,
            owner_id: 3,
            start_date: Date(),
            end_date: Date(),
            status: "PARTIAL",
            requests: [{request_id: 3, start_date: Date(), end_date: Date()}]
        },
        {
            offer_id: 4,
            owner_id: 4,
            start_date: Date(),
            end_date: Date(),
            status: "EXPIRED",
            requests: [{request_id: 4, start_date: Date(), end_date: Date()}]
        }
    ]
}
    */