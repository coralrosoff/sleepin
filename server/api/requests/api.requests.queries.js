/**
 * Created by user on 22/09/2016.
 */
    
module.exports={
    RequestBed:"\
    insert into requests values(@offerId,@userId,@beginDate,@endDate,'PENDING',GETDATE(),GETDATE());",
    RespondToRequest:"\
    update requests set request_status=@response where request_id=@requestId",
    CancelRequest:"\
    update requests set request_status='CANCELED' where request_id=@requestId",
    GetRequestDetails:"\
    SELECT requests.begin_date, requests.end_date, requests.request_status,\
           requesters.display_name requester_name, requesters.phone requester_phone, requesters.voip requester_voip,\
           owners.display_name owner_name, owners.phone owner_phone, owners.voip owner_voip\
    FROM sleepin.dbo.requests requests\
    LEFT OUTER JOIN sleepin.dbo.soldiers requesters\
    ON (requests.requester_id = requesters.soldier_id)\
    LEFT OUTER JOIN sleepin.dbo.offers offers\
    ON (requests.offer_id = offers.offer_id)\
    LEFT OUTER JOIN sleepin.dbo.soldiers owners\
    ON (offers.owner_id= owners.soldier_id)\
    WHERE requests.request_id = @request_id",
    DeclineOtherRequests:"\
    update sleepin.dbo.requests set request_status='DECLINED' where offer_id=@offer_id and request_id!=@request_id"

};
