/**
 * Created by user on 21/09/2016.
 */
module.exports = RequestsService;
var Q = require('q');
var queries = require('./api.requests.queries');
var config = require('../../config');
var sql = require('mssql');

function RequestsService() {
    var  mssqlConnectionConfig = config.mssqlConnectionDetails;
    var connection = new sql.Connection(mssqlConnectionConfig);
    connection.connect();

    this.RequestBed=RequestBed;
    this.RespondToRequest=RespondToRequest;
    this.CancelRequest=CancelRequest;
    this.GetRequestDetails=GetRequestDetails;
    this.DeclineOtherRequests=DeclineOtherRequests;

    function RequestBed(offerId,userId,beginTS,endTS){
        var beginDate = new Date(beginTS);
        var endDate = new Date(endTS);
        console.log("the request id is:",userId);
        return new sql
            .Request(connection)
            .input('offerId',sql.Int(),offerId)
            .input('userId',sql.Int(),userId)
            .input('beginDate',sql.DateTime(),beginDate)
            .input('endDate',sql.DateTime(),endDate)
            .query(queries.RequestBed);
    }


    function RespondToRequest(requestId,response,offer_id){
        console.log("the request id is:",requestId ,"and the respnnse is:",response);
        var updateQuery= new sql
            .Request(connection)
            .input('requestId',sql.Int(),requestId)
            .input('response',sql.NVarChar(50),response)
            .query(queries.RespondToRequest);
        if(response==='ACCEPTED'){
            return updateQuery.then(function(){
                console.log("going to cancel all request in ",offer_id , "execpt",requestId);
                return DeclineOtherRequests(offer_id,requestId);
            });
        }else{
                return updateQuery;
        }

    }

    function CancelRequest(requestId){
        return new sql
            .Request(connection)
            .input('requestId',sql.Int(),requestId)
            .query(queries.CancelRequest);
    }

    function GetRequestDetails(requestId){
        return new sql
            .Request(connection)
            .input('requestId',sql.Int(),requestId)
            .query(queries.GetRequestDetails);
    }

    function DeclineOtherRequests(offer_id,request_id){
        return new sql
            .Request(connection)
            .input('offer_id',sql.Int(),offer_id)
            .input('request_id',sql.Int(),request_id)
            .query(queries.DeclineOtherRequests);
    }

  

}