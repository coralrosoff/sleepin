/**
 * Created by user on 21/09/2016.
 */

module.exports = RequestsRouter;
var RequestsController = require('./api.requests.controller.js');

function RequestsRouter(){

    var controller = new RequestsController();
    var express = require('express');
    var router = express.Router();
    
    
    //POST
    router.post('/CancelRequest',CancelRequest);
    router.post('/RespondToRequest',RespondToRequest);
    router.post('/RequestBed',RequestBed);
    

    function RequestBed(req,res,next){
        var offerId=req.body.offerId;
        var userId=req.body.userId;
        var beginDate=req.body.beginDate;
        var endDate=req.body.endDate;
        res.resolveAndResponse(controller.RequestBed(offerId,userId,beginDate,endDate));
    }

    function CancelRequest(req,res,next){
        var requestId = req.body.requestId;
        res.resolveAndResponse(controller.CancelRequest(requestId));
    }

    function RespondToRequest(req,res,next){
        var requestId=req.body.requestId;
        var response=req.body.response;
        var offer_id=req.body.offer_id;
        console.log("the response is:",response);
        res.resolveAndResponse(controller.RespondToRequest(requestId,response,offer_id));
    }


    this.router = router;
}

