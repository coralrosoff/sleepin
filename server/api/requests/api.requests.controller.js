/**
 * Created by user on 21/09/2016.
 */

module.exports = RequestsController;
var Q = require('q');
var RequestsService = require('./api.requests.service.js');
var MailController = require('../mail/api.mail.controller');
var PersonController = require('../person/api.person.controller');

function RequestsController(){
  
    var service = new RequestsService();
    this.CancelRequest= CancelRequest;
    this.RespondToRequest=RespondToRequest;
    this.RequestBed=RequestBed;


    function RequestBed(offerId,userId,beginDate,endDate){
        var deferred = Q.defer();
        var requestStatus=service.RequestBed(offerId,userId,beginDate,endDate);
        deferred.resolve(requestStatus);
        return deferred.promise;
    }
    function CancelRequest(requestId){
        var deferred = Q.defer();
        var isSuccessful=service.CancelRequest(requestId);
        deferred.resolve(isSuccessful);
        return deferred.promise;
    }

    function RespondToRequest(requestId,response,offer_id){
        var deferred = Q.defer();
        var isSuccessful=service.RespondToRequest(requestId,response,offer_id);
        deferred.resolve(isSuccessful);
        return deferred.promise;
    }
    
}