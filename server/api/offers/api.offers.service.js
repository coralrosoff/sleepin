/**
 * Created by user on 21/09/2016.
 */
module.exports = OffersService;
var Q = require('q');
var queries = require('./api.offers.queries');
var config = require('../../config');
var sql = require('mssql');

function OffersService() {
    this.GetUserOffers = GetUserOffers;
    this.GetBedOffers = GetBedOffers;
    this.GetOfferData=GetOfferData;
    this.GetOfferRequests=GetOfferRequests;
    this.CancelOffer = CancelOffer;
    this.EditOffer = EditOffer;
    this.CreateOffer = CreateOffer;

    var mssqlConnectionConfig = config.mssqlConnectionDetails;
    var connection = new sql.Connection(mssqlConnectionConfig);
    connection.connect();

    function GetUserOffers(userId){
        var userOffers = new sql
            .Request(connection)
            .input('owner_id', sql.Int(), userId)
            .query(queries.GetUserOffers);
        return userOffers.then(function(result){
            return result;  
        });
    }

    function GetBedOffers(gender){
        var bedOffers = new sql
            .Request(connection)
            .input('gender', sql.VarChar(), gender)
            .query(queries.GetBedOffers);
        return bedOffers.then(function(result){
            return {offers: result};
        });
    }

    function GetOfferData(offerId){
        return new sql
            .Request(connection)
            .input('offerId',sql.Int,offerId)
            .query(queries.GetOfferData);
    }

    function GetOfferRequests(offerId){
        return new sql
            .Request(connection)
            .input('offerId',sql.Int,offerId)
            .query(queries.GetOfferRequests);
    }

    function CancelOffer(offerId){
        var bedOffers = new sql
            .Request(connection)
            .input('offer_id', sql.Int(), offerId)
            .input('new_status', sql.VarChar(), 'CANCELED')
            .query(queries.CancelOffer);
        return bedOffers.then(function(result){
            return result;
        });
    }

    function EditOffer(offerId, beginDate, endDate){
        var bedOffers = new sql
            .Request(connection)
            .input('offer_id', sql.Int(), offerId)
            .input('begin_date', sql.DateTime(), beginDate)
            .input('end_date', sql.DateTime(), endDate)
            .query(queries.EditOffer);
        return bedOffers.then(function(result){
            return result;
        });
    }

    function CreateOffer(ownerId, beginTS, endTS){
        var beginDate = new Date(beginTS);
        var endDate = new Date(endTS);
        var bedOffers = new sql
            .Request(connection)
            .input('owner_id', sql.Int(), ownerId)
            .input('begin_date', sql.DateTime(), beginDate)
            .input('end_date', sql.DateTime(), endDate)
            .input('offer_status', sql.VarChar(), 'OPEN')
            .query(queries.CreateOffer);
        return bedOffers.then(function(result){
            return result;
        });
    }
}