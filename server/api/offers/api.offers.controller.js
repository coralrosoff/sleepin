/**
 * Created by user on 21/09/2016.
 */

module.exports = OffersController;
var Q = require('q');
var OffersService = require('./api.offers.service.js');

function OffersController() {

    var service = new OffersService();

    this.GetUserOffers = GetUserOffers;
    this.GetBedOffers = GetBedOffers;
    this.GetOfferRequests = GetOfferRequests;
    this.CancelOffer = CancelOffer;
    this.EditOffer = EditOffer;
    this.CreateOffer = CreateOffer;


    function GetUserOffers(userId) {
        var deferred = Q.defer();
        service.GetUserOffers(userId).then(function(offers) {
            var offersPromises = offers.map(function(offer) {
                return service.GetOfferRequests(offer.offer_id);
            });
            Q.all(offersPromises).then(function(results) {
                deferred.resolve(results.map(function(request, index) {
                    offers[index].requests = request;
                    return offers[index];
                }));
            });
        });
        return deferred.promise;
    }

    function GetBedOffers(gender) {
        var deferred = Q.defer();
        service.GetBedOffers(gender).then(function(offers) {
            var offersPromises = offers.offers.map(function(offer) {
                return service.GetOfferRequests(offer.offer_id);
            });
            Q.all(offersPromises).then(function(results) {
                deferred.resolve(results.map(function(request, index) {
                    offers.offers[index].requests = request;
                    return offers.offers[index];
                }));
            });
        });
        return deferred.promise;
    }

    function GetOfferRequests(offerId) {
        var deferred = Q.defer();
        var offerData = service.GetOfferData(offerId);
        var requests = service.GetOfferRequests(offerId);
        Q.all([offerData, requests]).spread(function (offer, requestData) {
            offer[0].requests = requestData;
            deferred.resolve(offer[0]);
        });
        return deferred.promise;
    }

    function CancelOffer(offerId) {
        var deferred = Q.defer();
        var isSuccessful = service.CancelOffer(offerId);
        deferred.resolve(isSuccessful);
        return deferred.promise;
    }

    function EditOffer(offerId, beginDate, endDate, offerStatus) {
        var deferred = Q.defer();
        var isSuccessful = service.EditOffer(offerId, beginDate, endDate, offerStatus);
        deferred.resolve(isSuccessful);
        return deferred.promise;
    }

    function CreateOffer(ownerId, beginDate, endDate) {
        var deferred = Q.defer();
        var isSuccessful = service.CreateOffer(ownerId, beginDate, endDate);
        deferred.resolve(isSuccessful);
        return deferred.promise;
    }

}