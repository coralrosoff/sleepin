/**
 * Created by user on 21/09/2016.
 */

module.exports = OffersRoutes;
var OffersController = require('./api.offers.controller.js');

function OffersRoutes(){

    var controller = new OffersController();
    var express = require('express');
    var bodyParser = require('body-parser');

    var app = express();
    app.use(bodyParser.json());
    app.use(bodyParser.urlencoded({ extended: true }));

    var router = express.Router();

    //POST
    router.post('/CancelOffer',CancelOffer);
    router.post('/EditOffer',EditOffer);
    router.post('/CreateOffer',CreateOffer);

    //GET
    router.get('/GetUserOffers',GetUserOffers);
    router.get('/GetBedOffers',GetBedOffers);
    router.get('/GetOfferRequests',GetOfferRequests);

    function GetUserOffers(req,res){
        var userId = req.query.user_id;
        res.resolveAndResponse(controller.GetUserOffers(userId));
    }

    function GetBedOffers(req,res){
        var gender = req.query.gender;
        res.resolveAndResponse(controller.GetBedOffers(gender));
    }

    function GetOfferRequests(req,res){
        var offerId = req.query.offer_id;
        res.resolveAndResponse(controller.GetOfferRequests(offerId));
    }
    
    function CancelOffer(req,res){
        var offerId=req.body.offer_id;
        res.resolveAndResponse(controller.CancelOffer(offerId));
    }

    function EditOffer(req,res){
        var offerId=req.body.offer_id;
        var beginDate=req.body.begin_date;
        var endDate=req.body.end_date;
        var offerStatus=req.body.offer_status;
        res.resolveAndResponse(controller.EditOffer(offerId, beginDate, endDate, offerStatus));
    }
    
    function CreateOffer(req,res){
        var ownerId=req.body.owner_id;
        var beginDate=req.body.begin_date;
        var endDate=req.body.end_date;
        res.resolveAndResponse(controller.CreateOffer(ownerId, beginDate, endDate));
    }

    this.router = router;
}

