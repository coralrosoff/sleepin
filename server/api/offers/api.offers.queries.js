/**
 * Created by user on 22/09/2016.
 */
module.exports={
    GetUserOffers:"SELECT * FROM sleepin.dbo.offers WHERE owner_id = @owner_id AND offer_status != 'CANCELED'",
    GetBedOffers:"SELECT offers.*, soldiers.display_name, soldiers.email, soldiers.phone, soldiers.voip\
                  FROM sleepin.dbo.offers offers, sleepin.dbo.soldiers soldiers\
                  WHERE offers.offer_status in ('OPEN','PARTIAL')\
                  AND soldiers.soldier_id = offers.owner_id and soldiers.gender = @gender",
    GetOfferData:"\
    select offers.owner_id,offers.begin_date,offers.end_date,offer_status from sleepin.dbo.offers \
    where offer_id=@offerId",
    GetOfferRequests:"\
    select requests.request_id,soldiers.user_name,soldiers.display_name,requests.requester_id,requests.begin_date,requests.end_date,\
    request_status,requests.creation_date,requests.last_modify_date\
    from sleepin.dbo.requests requests,sleepin.dbo.soldiers soldiers where requests.offer_id=@offerId\
    and requests.requester_id=soldiers.soldier_id\
    and requests.request_status != 'CANCELED'",
    CancelOffer:"UPDATE sleepin.dbo.offers\
                 SET offer_status = @new_status\
                 WHERE offer_id = @offer_id",
    EditOffer:"UPDATE sleepin.dbo.offers\
               SET begin_date = @begin_date,\
                   end_date = @end_date,\
                   last_modify_date = GETDATE()\
               WHERE offer_id = @offer_id",
    CreateOffer:"INSERT INTO sleepin.dbo.offers(owner_id, begin_date, end_date,\
                                                offer_status, creation_date, last_modify_date)\
                 VALUES (@owner_id, @begin_date, @end_date, @offer_status, GETDATE(), GETDATE())"
};