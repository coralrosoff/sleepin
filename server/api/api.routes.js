/**
 * Created by user on 21/09/2016.
 */
var express = require('express');
var router = express.Router();

//var LoginRoutes = require('./login/api.login.routes.js');
var BedsRoutes = require('./beds/api.beds.routes.js');
var RequestsRoutes = require('./requests/api.requests.routes.js');
var OffersRoutes = require('./offers/api.offers.routes.js');
var PersonRoutes = require('./person/api.person.routes.js');

var bedsRoutes = new BedsRoutes();
var offersRoutes = new OffersRoutes();
var personRoutes = new PersonRoutes();
var requestsRoutes=new RequestsRoutes();
//var loginRoutes= new LoginRoutes();

//router.use('/login', loginRoutes.router);
router.use('/beds', bedsRoutes.router);
router.use('/offers', offersRoutes.router);
router.use('/person', personRoutes.router);
router.use('/requests', requestsRoutes.router);


module.exports = router;
