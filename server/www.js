var express  = require('express');
var cors  = require("cors");
var path = require('path');
var bodyParser = require('body-parser');
var api = require('./api/api.routes');
var responseHandler = require('./middlewares/responseHandler');
var app = express();


//when we get a request , we first do this and then do the express.static function
/*app.use(function(req,res,next){
    console.log('${req.method} request for ' + ${req.url} + ' -${JSON.stringify((req.body))}');
    next();
});*/

app.use(bodyParser.json());
app.use(bodyParser.urlencoded({extended:true}));

app.use(responseHandler);
app.use('/api', api);
app.use(cors());


// get a path to a directory which from we would like to serve static files
app.use(express.static('../client'));
app.use(express.static(path.join(__dirname, '..', 'client', 'main')));
app.use(express.static(path.join(__dirname, '..', 'client', 'app')));
app.use(express.static(path.join(__dirname, '..', 'client', 'lib')));



app.listen(3000);
console.log("SleepIn app running on port 3000");

module.exports = app;
